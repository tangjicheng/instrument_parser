use std::fs::File;
use std::io::{BufRead, BufReader};
use regex::Regex;

fn main() -> std::io::Result<()> {
    let log_file_path = "/Users/tangjicheng/proj/instrument_parser/matching_engine_20240503.log";
    let file = File::open(log_file_path)?;
    let reader = BufReader::new(file);

    // Regular expression to capture Instrument ID
    let re = Regex::new(r"DbLoadInstrumentCmd::execute \d+ Instrument (\w+) loaded").unwrap();

    // Counter to limit the number of lines processed
    let mut counter = 0;
    const MAX_LINES: usize = 20000; // Maximum number of lines to read

    for line in reader.lines() {
        if counter >= MAX_LINES {
            break;  // Stop reading after 20,000 lines
        }

        let line = line?;
        
        if let Some(captures) = re.captures(&line) {
            if let Some(instr) = captures.get(1) {
                println!("{}", instr.as_str());
            }
        }

        counter += 1; // Increment the counter with each processed line
    }

    Ok(())
}
