FROM rust:1.78-bookworm AS builder
WORKDIR /build
COPY . .
RUN cargo build --release

FROM rust:1.78-bookworm
WORKDIR /app
COPY --from=builder /build/target/release/instrument_parser ./
CMD [ "/app/instrument_parser" ]